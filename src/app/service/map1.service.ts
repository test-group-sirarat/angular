import { ElementRef, Injectable } from '@angular/core';
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import Graphic from "@arcgis/core/Graphic";
import SimpleMarkerSymbol from "@arcgis/core/symbols/SimpleMarkerSymbol";
import MapImageLayer from "@arcgis/core/layers/MapImageLayer";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import Point from "@arcgis/core/geometry/Point";
import * as identify from "@arcgis/core/rest/identify";
import IdentifyParameters from "@arcgis/core/rest/support/IdentifyParameters";1
import { observable, Observable } from 'rxjs';
import Geometry from "@arcgis/core/geometry/Geometry";
import Polygon from "@arcgis/core/geometry/Polygon";
import * as geometryEngine from "@arcgis/core/geometry/geometryEngine";
import Editor from "@arcgis/core/widgets/Editor";
import FeatureSet from "@arcgis/core/rest/support/FeatureSet";
import ClosestFacilityParameters from "@arcgis/core/rest/support/ClosestFacilityParameters";
import * as closestFacility from "@arcgis/core/rest/closestFacility";



@Injectable({
  providedIn: 'root'
})
export class MapService {

  map!: Map
  mapView!: MapView
  mapImgLayer!: MapImageLayer
  featureLayer!: FeatureLayer
  state: string[] = []
  subRegion: string[] = []
  shopfeatureLayer!: FeatureLayer

  data: { region: string, state_name: string, abbr: string }[] = []

  constructor() { }

  initialMap(dom: ElementRef) {
    this.map = new Map({
      basemap: "topo-vector"
    })

    this.mapView = new MapView({
      map: this.map,
      container: dom.nativeElement,
      center: [100.5, 13.7],
      zoom: 6
    })
    const layer = new MapImageLayer({
      url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer"
    })

    this.map.add(layer)

    this.mapImgLayer = new MapImageLayer({
      url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer"

    })
    this.map.add(this.mapImgLayer)

    this.featureLayer = new FeatureLayer({
      url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer/2"

    })

    this.shopfeatureLayer = new FeatureLayer({
      url: "https://gisserv1.cdg.co.th/arcgis/rest/services/AtlasX/AtlasX/FeatureServer/0"
    })


    this.map.add(this.shopfeatureLayer)
    this.mapView.when(() => {
      //  this.queryAllState()
      this.initialEditor()
      this.mapView.on("click", (clickResponse) => {
        this.mapView.graphics.removeAll()

        console.log("OnMapClick clickResponse=", clickResponse)
        const graphicInfident = this.drawPointToMap(clickResponse.mapPoint)
        this.mapView.graphics.add(graphicInfident)
        this.doBuffer(clickResponse.mapPoint, graphicInfident)
      })
    })
  }


  initialEditor() {
    const editor = new Editor({
      view: this.mapView
    });
    this.mapView.ui.add(editor, "top-right")
  }
  doBuffer(point: Point, graphicInfident: Graphic) {
    const bufferGP = geometryEngine.buffer(point, 100)
    console.log(" doBuffer bufferGP", bufferGP)
    this.addPolygonToGraphicLayer(bufferGP as Geometry)
    this.spatialQuery(bufferGP as Geometry, graphicInfident)

  }
  spatialQuery(geometry: Geometry, graphicInfident: Graphic) {
    let params = this.shopfeatureLayer.createQuery()
    params.geometry = geometry
    params.outFields = ["*"]
    params.returnGeometry = true

    this.shopfeatureLayer.queryFeatures(params).then((response) => {
      console.log("response", response)


      let params = new ClosestFacilityParameters({
        returnIncidents: true,
        returnFacilities: true,
        returnRoutes: true,
        returnDirections: true,
        defaultTargetFacilityCount: 5,
        incidents: new FeatureSet({
          features: [graphicInfident]
        }),
        facilities: new FeatureSet({
          features: response.features
        })
      })
      closestFacility.solve("https://gisserv1.cdg.co.th/arcgis/rest/services/SI_Network/NAServer/Closest%20Facility", params).then((response) => {
        console.log("ClosetFac", response)
        if (response.routes.features.length > 0) {
          for (let feature of response.routes.features) {
            console.log("feature = ", feature)
            let symbol = {
              type: "simple-line",
              color: "lightblue",
              width: "2px",
              style: "short-dot"
            };
            const graphicRoute = new Graphic({
              geometry: feature.geometry,
              symbol: symbol
            })
            this.mapView.graphics.add(graphicRoute)
          }
        }
      })
    })
  }
  drawPointToMap(point: Point) {
    this.mapView.graphics.removeAll()
    const symbol = new SimpleMarkerSymbol({
      style: "circle",
      color: [0, 255, 0, 0, 0.5],
      size: "15px", //pixels
      outline: {//autocasts as new SimpleLineSymbol()
        color: [255, 0, 0],
        width: 2 //points
      }
    })
    const graphic = new Graphic({
      geometry: point,
      symbol: symbol
    })
    // this.mapView.graphics.add(graphic)
    return graphic
  }
  identify(geometry: Geometry) {
    let params = new IdentifyParameters()
    params.tolerance = 3
    params.layerIds = [2];
    params.geometry = geometry
    params.width = this.mapView.width
    params.height = this.mapView.height
    params.mapExtent = this.mapView.extent
    identify.identify(this.mapImgLayer.url, params).then((response) => {
      console.log("identify response", response)
      if (response.results.length > 0) {
        let feature = response.results[0].feature
        feature.popupTemplate = {
          title: "{state_name}",
          content: "Population: {pop2000}<br>  Area: {st_area(shape)} SQKM"
        }
      }
    })
  }
  goTo(lat: number, long: number) {
    this.mapView.goTo({
      center: [long, lat],
      zoom: 16
    }, {
      duration: 3000
    })
    this.mapView.graphics.removeAll()
    //Add graphic
    const point = new Point({
      latitude: lat,
      longitude: long
    })
    const symbol = new SimpleMarkerSymbol({
      style: "circle",
      color: [0, 255, 0, 0, 0.5],
      size: "15px", //pixels
      outline: {//autocasts as new SimpleLineSymbol()
        color: [255, 0, 0],
        width: 3 //points
      }
    })
    const graphic = new Graphic({
      geometry: point,
      symbol: symbol
    })
    this.mapView.graphics.add(graphic)
    this.queryStateByStateName('Hawaii')
  }
  queryAllState() {
    let query = this.featureLayer.createQuery();
    query.where = "1=1"
    query.outFields = ["sub_region", "state_name", "state_abbr"]
    query.returnGeometry = false
    this.featureLayer.queryFeatures(query).then((response) => {
      console.log("response = ", response)
    })
  }
  queryAllStateObs(): Observable<any> {
    let query = this.featureLayer.createQuery();
    query.where = "1=1"
    query.outFields = ["sub_region", "state_name", "state_abbr"]
    query.returnGeometry = false
    const Obs = new Observable(observer => {
      this.featureLayer.queryFeatures(query).then((response) => {
        console.log("##### response =", response)
        if (response.features.length > 0) {
          for (let feature of response.features) {
            this.data.push({ region: feature.attributes.sub_region, state_name: feature.attributes.state_name, abbr: feature.attributes.state_abbr })
          }
        }
        observer.next(this.data)
        observer.complete
      })
    });
    return Obs
  }
  queryStateByStateName(stateName: string) {
    let query = this.featureLayer.createQuery();
    query.where = "state_name ='" + stateName + "'"
    query.outFields = ["sub_region", "state_name", "state_abbr"]
    query.returnGeometry = true
    return this.featureLayer.queryFeatures(query)
  }
  addPolygonToGraphicLayer(polygon: Geometry) {
    let symbol = {
      type: "simple-fill",
      color: [51, 51, 204, 0.9],
      styles: "solid",
      outline: {
        color: "white",
        with: 1
      }
    };
    const graphic = new Graphic({
      geometry: polygon,
      symbol: symbol
    })
    let polygon2 = polygon as Polygon
    this.mapView.graphics.removeAll()
    this.mapView.graphics.add(graphic)
  }
}