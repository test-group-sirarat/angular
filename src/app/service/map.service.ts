import { ElementRef, Injectable } from '@angular/core';
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import Graphic from "@arcgis/core/Graphic";
import SimpleMarkerSymbol from "@arcgis/core/symbols/SimpleMarkerSymbol";
import MapImageLayer from "@arcgis/core/layers/MapImageLayer";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import Point from "@arcgis/core/geometry/Point";
import * as identify from "@arcgis/core/rest/identify";
import IdentifyParameters from "@arcgis/core/rest/support/IdentifyParameters";
import FeatureTable from "@arcgis/core/widgets/FeatureTable";
import { observable, Observable } from 'rxjs';
import Geometry from "@arcgis/core/geometry/Geometry";
import Polygon from "@arcgis/core/geometry/Polygon";
import LayerList from "@arcgis/core/widgets/LayerList";


@Injectable({
  providedIn: 'root'
})
export class MapService {

  map!: Map
  mapView!: MapView
  featureLayer!: FeatureLayer
  state: string[] = []
  subRegion: string[] = []

  data: {region:string, state_name:string, abbr:string}[] = []


  



  constructor() { }

  initialMap(dom: ElementRef) {
    this.map = new Map({
      basemap: "topo-vector"
    })

    this.mapView = new MapView({
      map: this.map,
      container: dom.nativeElement,
      center: [	-89.500000, 	44.500000],
      zoom: 6
    })
    const layer = new MapImageLayer({
      url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer"
    })

    this.map.add(layer)

    this.featureLayer = new FeatureLayer({
      url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer/2"

    })
    this.mapView.when(() => {
      this.initialLayerlist()
      //  this.queryAllState()
      this.mapView.on("click", (clickResponse) => {
        console.log("clickResponse", clickResponse)
        let params = new IdentifyParameters()
        params.tolerance = 3
        params.layerIds = [2];
        params.geometry = clickResponse.mapPoint
        params.width = this.mapView.width
        params.height = this.mapView.height
        params.mapExtent = this.mapView.extent

        identify.identify(layer.url, params).then((response) => {
          console.log("identify response", response)

          if (response.results.length > 0) {
            let feature = response.results[0].feature
            feature.popupTemplate = {
              title: "{state_name}",
              content: "Population: {pop2000}<br>  Area: {st_area(shape)} SQKM"
            }
            //show popup
            this.mapView.popup.open({
              features: [feature],
              location: clickResponse.mapPoint
            })
          }
        })
      })      
    })
  }

  initialLayerlist(){
    let layerList = new LayerList({
      view: this.mapView
    });
    this.mapView.ui.add(layerList, {
      position: "top-left"
    });
  }

  goTo(lat: number, long: number) {
    this.mapView.goTo({
      center: [long, lat],
      zoom: 16
    }, {
      duration: 3000
    })

    this.mapView.graphics.removeAll()
    //Add graphic
    const point = new Point({
      latitude: lat,
      longitude: long
    })

    const symbol = new SimpleMarkerSymbol({
      style: "circle",
      color: [0, 255, 0, 0, 0.5],
      size: "15px", //pixels
      outline: {//autocasts as new SimpleLineSymbol()
        color: [255, 0, 0],
        width: 3 //points
      }
    })

    const graphic = new Graphic({
      geometry: point,
      symbol: symbol
    })
    this.mapView.graphics.add(graphic)
    this.queryStateByStateName('Hawaii')
  }

  queryAllState() {
    let query = this.featureLayer.createQuery();
    query.where = "1=1"
    query.outFields = ["sub_region", "state_name", "state_abbr"]
    query.returnGeometry = false

    this.featureLayer.queryFeatures(query).then((response) => {
      console.log("response = ", response)
    })
  }

  queryAllStateObs(): Observable<any>{
    let query = this.featureLayer.createQuery();
      query.where = "1=1"
      query.outFields = ["sub_region","state_name","state_abbr"]
      query.returnGeometry = false

      const Obs = new Observable(observer =>{
        this.featureLayer.queryFeatures(query).then((response)=> {
          console.log("##### response =" , response)

          if(response.features.length > 0){
            for(let feature of response.features){
               this.data.push({region: feature.attributes.sub_region , state_name:feature.attributes.state_name ,abbr:feature.attributes.state_abbr})
              
            }
            
          }
          observer.next(this.data)
          observer.complete
        })
    });
    return Obs
   }
   
  

  queryStateByStateName(stateName: string) {
    let query = this.featureLayer.createQuery();
    query.where = "state_name ='" + stateName + "'"
    query.outFields = ["sub_region", "state_name", "state_abbr"]
    query.returnGeometry = true

    // this.featureLayer.queryFeatures(query).then((response) => {
    //   console.log("response = ", response)
    // })

    return this.featureLayer.queryFeatures(query)
  }

  addPolygonToGraphicLayer(polygon: Geometry){
    let symbol = {
      type: "simple-fill" , 
      color: [51,51,204,0.9],
      styles: "solid",
      outline:{
        color: "white",
        with: 1
      }
    };


    const graphic = new Graphic({
      geometry: polygon,
      symbol: symbol
    })

    let polygon2 = polygon as Polygon
    this.mapView.graphics.removeAll()

    this.mapView.graphics.add(graphic)
    this.mapView.goTo({
      center:[polygon2.centroid.longitude, polygon2.centroid.latitude],
      zoom: 6
          
    })

    
  }
 

  
}