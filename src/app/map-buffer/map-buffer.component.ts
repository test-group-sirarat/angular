import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from '../service/map1.service';

@Component({
  selector: 'app-map-buffer',
  templateUrl: './map-buffer.component.html',
  styleUrls: ['./map-buffer.component.css']
})
export class MapBufferComponent implements OnInit {

  @ViewChild('divMap') divMap!: ElementRef
  states: {region:string, state_name:string, abbr:string}[] = []
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }

  onlocatorClick(data: any){
    console.log(data)
  }

  ngAfterViewInit(){
    this.mapService.initialMap(this.divMap)
    this.mapService.queryAllStateObs().subscribe((response)=>{
      console.log(response)
      this.states = response
    })
  }


}
