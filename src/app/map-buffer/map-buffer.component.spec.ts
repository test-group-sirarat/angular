import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapBufferComponent } from './map-buffer.component';

describe('MapBufferComponent', () => {
  let component: MapBufferComponent;
  let fixture: ComponentFixture<MapBufferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapBufferComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapBufferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
