import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MapService } from 'src/app/service/map.service';


@Component({
  selector: 'app-locate',
  templateUrl: './locate.component.html',
  styleUrls: ['./locate.component.css']
})
export class LocateComponent implements OnInit {
  @Output() locator = new EventEmitter()
  latitude: number = 0
  longtitude: number = 0
  constructor(private mapService:MapService) { }

  ngOnInit(): void {
  }

  onClickLocate() {
    this.mapService.goTo(Number(this.latitude), Number(this.longtitude))
  }
}