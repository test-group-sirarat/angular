import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from '../service/map.service';
@Component({
  selector: 'app-assignment2',
  templateUrl: './assignment2.component.html',
  styleUrls: ['./assignment2.component.css']
})
export class Assignment2Component implements OnInit {
  
  @ViewChild('divMap') divMap!: ElementRef
  states!: string[]
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }

  onlocatorClick(data: any){
    console.log(data)
  }

  ngAfterViewInit(){
    this.mapService.initialMap(this.divMap)
    this.mapService.queryAllStateObs().subscribe((response)=>{
      console.log(response)
      this.states = response
    })
  }

  doStateClick(stateName: string){
    this.mapService.queryStateByStateName(stateName).then((response)=>{
      console.log("queryStateBystateName response=", response)
      if(response.features.length>0){
        let feature = response.features[0]

        this.mapService.addPolygonToGraphicLayer(feature.geometry)
        
      }
    })
  }
 


}