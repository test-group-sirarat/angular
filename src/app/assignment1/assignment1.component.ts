import { Component, OnInit } from '@angular/core';
import { HoraService } from '../service/hora.service';

@Component({
  selector: 'app-assignment1',
  templateUrl: './assignment1.component.html',
  styleUrls: ['./assignment1.component.css']
})
export class Assignment1Component implements OnInit {
  commentList: comment[] = []
  comment: string = ""
  name : string = ""
  constructor() {}

  ngOnInit(): void {
  }
  doComment(){
    this.commentList.push({name: this.name , comment: this.comment})
  }
  clearComment(){
    this.comment = ""
    this.name = ""
   }
}

interface comment{
  name : string,
  comment : string
}

