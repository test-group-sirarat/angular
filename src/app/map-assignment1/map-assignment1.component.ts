import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from '../service/map.service';

@Component({
  selector: 'app-map-assignment1',
  templateUrl: './map-assignment1.component.html',
  styleUrls: ['./map-assignment1.component.css']
})
export class MapAssignment1Component implements OnInit {

  @ViewChild('divMap') divMap!: ElementRef
  states: {region:string, state_name:string, abbr:string}[] = []
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }

  onlocatorClick(data: any){
    console.log(data)
  }

  ngAfterViewInit(){
    this.mapService.initialMap(this.divMap)
    this.mapService.queryAllStateObs().subscribe((response)=>{
      console.log(response)
      this.states = response
    })
  }
  doStateClick(stateName: string){
    this.mapService.queryStateByStateName(stateName).then((response)=>{
      console.log("queryStateBystateName response=", response)
      if(response.features.length>0){
        let feature = response.features[0]

        this.mapService.addPolygonToGraphicLayer(feature.geometry)
        
      }
    })
  }



 

}
