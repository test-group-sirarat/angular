import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapAssignment1Component } from './map-assignment1.component';

describe('MapAssignment1Component', () => {
  let component: MapAssignment1Component;
  let fixture: ComponentFixture<MapAssignment1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapAssignment1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapAssignment1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
