import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Assignment1Component } from './assignment1/assignment1.component';
import { Assignment3Component } from './assignment3/assignment3.component';
import { Assignment2Component } from './assignment2/assignment2.component';
import { MapAssignment1Component } from './map-assignment1/map-assignment1.component';
import { MapBufferComponent } from './map-buffer/map-buffer.component';
import { ResponsiveComponent } from './responsive/responsive.component';

const routes: Routes = [
  {path: 'assignment1' , component: Assignment1Component},
  {path: 'assignment3' , component: Assignment3Component},
  {path: 'assignment2' , component: Assignment2Component},
  {path: 'Mapassignment1' , component: MapAssignment1Component},
  {path: 'MapBuffer' , component: MapBufferComponent},
  {path: 'Responsive' , component:  ResponsiveComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
