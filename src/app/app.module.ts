import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatsComponent } from './cats/cats.component';
import { FormsModule } from '@angular/forms';
import { Assignment1Component } from './assignment1/assignment1.component';
import { CommentComponent } from './assignment1/comment/comment.component';
import { Assignment3Component } from './assignment3/assignment3.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import { LocateComponent } from './assignment2/locate/locate.component';
import { Assignment2Component } from './assignment2/assignment2.component';
import { MapAssignment1Component } from './map-assignment1/map-assignment1.component';
import { MapBufferComponent } from './map-buffer/map-buffer.component';
import { ResponsiveComponent } from './responsive/responsive.component';


@NgModule({
  declarations: [
    AppComponent,
    CatsComponent,
    Assignment1Component,
    CommentComponent,
    Assignment3Component,
    LocateComponent,
    Assignment2Component,
    MapAssignment1Component,
    MapBufferComponent,
    ResponsiveComponent,
  ],
  imports: [
    AppRoutingModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ButtonModule,
    InputTextModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
